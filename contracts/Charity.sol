//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.10;

contract Charity {
  struct Donation {
    bool added;
    uint256 balance;
  }

  address private owner;
  address[] private donaters;
  mapping(address => Donation) private donates;

  /// Incorrect value {value} was given
  error IncorrectValue(uint256 value);

  modifier onlyOwner() {
    require(msg.sender == owner);
    _;
  }

  constructor(address _owner) {
    owner = _owner;
  }

  function getDonaters() public view returns (address[] memory) {
    return donaters;
  }

  function getDonatesOf(address _address) public view returns (uint256) {
    return donates[_address].balance;
  }

  receive() external payable {
    Donation storage donation = donates[msg.sender];
    if (msg.value + donation.balance <= donation.balance) {
      revert IncorrectValue(msg.value);
    }
    donation.balance += msg.value;

    if (!donation.added) {
      donation.added = true;
      donaters.push(msg.sender);
    }
  }

  function withdraw(address payable _address, uint256 amount) public onlyOwner payable {
    _address.transfer(amount);
  }
}
