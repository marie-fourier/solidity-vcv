require("@nomiclabs/hardhat-waffle");
require('solidity-coverage');
require('dotenv').config();

const getCharityContract = async () => {
  const address = '0x353DAa5AfCb1c688a3F3B63621AA67124c9A3aa8';
  const Charity = await ethers.getContractFactory('Charity');
  return await Charity.attach(address);
}

task('donaters', "Prints the list of donaters", async () => {
  const contract = await getCharityContract();
  const donaters = await contract.getDonaters();
  for (const address of donaters) {
    console.log(address);
  }
});

task('donation', "Prints the donation of given address")
  .addParam('address', 'Address')
  .setAction(async (taskArgs) => {
    const contract = await getCharityContract();
    const balance = (await contract.getDonatesOf(taskArgs.address)).toString();
    console.log(ethers.utils.formatEther(balance), "ETH");
  });

task('donate', "Donates given amount to charity")
  .addParam('amount', 'Amount in ether')
  .setAction(async (taskArgs) => {
    const contract = await getCharityContract();
    const [owner] = await ethers.getSigners();
    const balance = (await ethers.provider.getBalance(owner.address)).toString();
    const bBalance = ethers.BigNumber.from(balance);
    const bAmount = ethers.BigNumber.from(ethers.utils.parseEther(taskArgs.amount));

    if (bBalance.lt(bAmount)) {
      console.log('Wrong amount - balance is less than amount');
      return;
    }

    const trx = await owner.sendTransaction({
      to: contract.address,
      value: bAmount,
    });
    await trx.wait();

    console.log(`${taskArgs.amount} ETH sent to charity. Trx hash: ${trx.hash}`);
  });

task('withdraw', "Withdraws given amount to given address")
  .addParam('amount', 'Amount in ether')
  .addParam('address', 'Recipient address')
  .setAction(async (taskArgs) => {
    const contract = await getCharityContract();
    const [owner] = await ethers.getSigners();
    const balance = (await ethers.provider.getBalance(contract.address)).toString();
    const bBalance = ethers.BigNumber.from(balance);
    const bAmount = ethers.BigNumber.from(ethers.utils.parseEther(taskArgs.amount));

    if (bAmount.gt(bBalance)) {
      console.log('Wrong amount - balance is less than amount');
      return;
    }

    const trx = await contract.connect(owner).withdraw(taskArgs.address, bAmount);
    await trx.wait();

    console.log(`Withdrew ${taskArgs.amount} ETH to ${taskArgs.address}. Trx hash: ${trx.hash}`);
  });

const { RINKEBY_ENDPOINT, PRIVATE_KEY } = process.env;

module.exports = {
  solidity: "0.8.10",
  plugins: [
    'solidity-coverage',
  ],
  networks: {
    rinkeby: {
      url: RINKEBY_ENDPOINT,
      accounts: [`0x${PRIVATE_KEY}`],
    },
  },
};
