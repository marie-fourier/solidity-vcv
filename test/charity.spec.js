const { expect } = require('chai');
const { ethers, waffle } = require('hardhat');

describe('Charity', function () {
  before(async () => {
    [owner, addr1, addr2] = await ethers.getSigners();
    CharityFactory = await ethers.getContractFactory('Charity');
    charity = await CharityFactory.deploy(owner.address);
    await charity.deployed();
  });

  it('should save donation and donaters address in storage', async () => {
    const value = ethers.utils.parseEther('1');
    await addr1.sendTransaction({
      to: charity.address,
      value,
    });

    const balance = (await charity.getDonatesOf(addr1.address)).toString();
    expect(balance).to.equal(value);

    const donaters = (await charity.getDonaters());
    expect(donaters).to.have.members([addr1.address]);
  });

  it('should not duplicate donaters addresses', async () => {
    const value = ethers.utils.parseEther('1');
    const valueTotal = ethers.utils.parseEther('2');
    await addr2.sendTransaction({
      to: charity.address,
      value,
    });
    await addr2.sendTransaction({
      to: charity.address,
      value,
    });

    const balance = (await charity.getDonatesOf(addr2.address)).toString();
    expect(balance).to.equal(valueTotal);

    const donaters = (await charity.getDonaters());
    expect(donaters.filter(addr => addr == addr2.address)).to.have.length(1);
  });

  it('should not accept zero value', async () => {
    const value = ethers.utils.parseEther('0');
    await expect(
      addr1.sendTransaction({
        to: charity.address,
        value,
      })
    ).to.be.revertedWith(`IncorrectValue(${value})`);
  });

  it('should let only owner to withdraw', async () => {
    const value = ethers.utils.parseEther('1');
    await expect(charity.connect(addr1).withdraw(addr1.address, value)).to.be.reverted;
    await expect(charity.connect(addr2).withdraw(addr1.address, value)).to.be.reverted;
    await expect(charity.withdraw(owner.address, value)).to.not.be.reverted;
  });

  it('should increase contracts balance', async () => {
    const value = ethers.utils.parseEther('1');
    await expect(await addr1.sendTransaction({
      to: charity.address,
      value,
    })).to.changeEtherBalance(addr1, `-${value}`);

    await expect(await addr1.sendTransaction({
      to: charity.address,
      value,
    })).to.changeEtherBalance(charity, value);
  });

  it('should send all the balance to given address', async () => {
    const provider = waffle.provider;
    const balance = await provider.getBalance(charity.address);

    await expect(await charity.withdraw(owner.address, balance))
      .to.changeEtherBalance(owner, balance);
  });
});
