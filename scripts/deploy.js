const hre = require("hardhat");

async function main() {
  await hre.run('compile');

  const [deployer] = await ethers.getSigners();

  console.log("Deploying contracts with the account:", deployer.address);

  const Charity = await hre.ethers.getContractFactory("Charity");
  const charity = await Charity.deploy(deployer.address);
  
  await charity.deployed();

  console.log("Charity deployed to:", charity.address);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
